# SOSCity - Versão Web 

Instruções do projeto (front-end):

1- Abrir o terminal do seu computador (prompt de comando), ou o Git Bash, caso o tenha (para abrir o Git Bash, basta clicar com o botão direito na pasta desejada e, verá a opção GIT BASH HERE. **Clicar nela**).
Para abrir o prompt de comando do seu computador, basta apertar em seu teclado a tecla do Windows (geralmente perto do backspace, ou espaço, do seu teclado) e procurar por "CMD" ou "Prompt de comando", conforme fotos: 
https://drive.google.com/file/d/1CDvXeW7ESzDuKEa1HUfhNOB5qEjp_s5Z/view?usp=sharing
https://drive.google.com/file/d/1gZ7mBx9_DKXmxuhrFa8vlm3Qd6n-IydC/view?usp=sharing

2- Digitar o seguinte comando **no prompt de comando ou git bash**, dentro da pasta criada, para o projeto:

git clone https://gitlab.com/Lbm99/soscity-versao-web.git 
e, apertar ENTER.

Assim, o clone do mesmo será feito, conforme imagem: 
https://drive.google.com/file/d/1IABlh58UaXphQF6MShQuIqt7VNNT-QwZ/view?usp=sharing


3- Se tudo correr bem, abrirá o projeto em seu computador e, o arquivo index.html será o principal do sistema, disponível nas pastas iniciais. 
O resultado deve ser igual ao do link da foto: 
https://drive.google.com/file/d/1IABlh58UaXphQF6MShQuIqt7VNNT-QwZ/view?usp=sharing


4 - As explicações de como funciona a divisão de pastas e da lógica do negócio estão dispostas no arquivo a seguir: 
https://drive.google.com/file/d/17eNaZROJgnNLBq1GaUw4dBZWPCezH7Mu/view?usp=sharing
