<?php
header('Access-Control-Allow-Origin: *');

class App extends CI_Controller {

 function index()
  {
   $this->load->view('index.html');
  }
  
  public function view($page)
    {
        //echo (!$this->session->userdata('user'));
        if(!$this->session->userdata('user') && $page != 'login') {
	       $this->session->sess_destroy();  
           redirect(base_url() . 'app/view/login');
        }
        $this->load->view('pages/'.$page.'.html');
    }
}
?>