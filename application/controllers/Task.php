<?php
header('Access-Control-Allow-Origin: *');
defined('BASEPATH') OR exit('No direct script access allowed');

class Task extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Task_model');
		$this->load->library('form_validation');
	}

	function index()
	{
		$data = $this->Task_model->fetch_all();
		echo json_encode($data->result_array());
	}
	
	function user()
	{
	    $id = $this->input->post('id');
	    if($this->input->post('id') == 1 && $this->session->userdata('user') != null) $id = $this->session->userdata('user');
	    //echo $id;
		$data = $this->Task_model->fetch_user($id);
		echo json_encode($data->result_array());
	}

	function insert()
	{
	    //$data = $this->input;
		//var_dump($_POST);
		//var_dump($_GET);
	    
	    $idUser = $this->input->post('IdUser');
	    $endereco = $this->input->post('endereco');
	    $categoria = $this->input->post('categoria');
	    
	    if($this->session->userdata('user')) $idUser = $this->session->userdata('user');
	    if($this->input->post('enderecoNovo')) $endereco = $this->input->post('enderecoNovo');
	    if(!$categoria) $categoria = 0;
	    
	    if(!$idUser) {
	        echo(json_encode("{'error': 'idUser is required'}"));
	        return 0;
	    }
	    if(!$endereco){
	        echo(json_encode("{'error': 'endereco is required'}"));
	        return 0;
	    }
	    
 		//$this->form_validation->set_rules('IdUser', 'IdUser', 'required');
		$this->form_validation->set_rules('titulo', 'Titulo', 'required');
		$this->form_validation->set_rules('descricao', 'Descricao', 'required');
 		//$this->form_validation->set_rules('categoria', 'Categoria', 'required');
		$this->form_validation->set_rules('MinParticipantes', 'MinParticipantes', 'required');
		$this->form_validation->set_rules('encerramento', 'Encerramento', 'required');
		//$this->form_validation->set_rules('endereco', 'Endereco', 'required');


		if($this->form_validation->run())
		{
			$data = array(
			    'titulo'	=>	$this->input->post('titulo'),
				'descricao'	=>	$this->input->post('descricao'),
				'MinParticipantes'	=>	$this->input->post('MinParticipantes'),
		        'encerramento'	=>	$this->input->post('encerramento'),
		        'categoria'	=>	$categoria,
		        'endereco'	=>	$endereco,
				'IdUser'	=>	$idUser,
				//'IdUser'		=>	form_error('IdUser')
			);

			$this->Task_model->insert_Task($data);

			$array = array(
				'success'		=>	true
			);
		}
		else
		{
			$array = array(
				'error'					=>	true,
				'titulo'			=>	form_error('titulo'),
				'descricao'			=>	form_error('descricao'),
				'MinParticipantes'			=>	form_error('MinParticipantes'),
				'categoria'			=>	form_error('categoria'),
			    'encerramento'	=>	form_error('encerramento'),
			    'endereco'			=>	form_error('endereco'),
				'IdUser'		=>	form_error('IdUser')

			);
		}
		echo json_encode($array);
	}
	
	function update()
	{
		$this->form_validation->set_rules('IdTask', 'IdTask', 'required');
		$this->form_validation->set_rules('titulo', 'Titulo', 'required');
		$this->form_validation->set_rules('descricao', 'Descricao', 'required');
		$this->form_validation->set_rules('MinParticipantes', 'MinParticipantes', 'required');
		$this->form_validation->set_rules('encerramento', 'Encerramento', 'required');
		$this->form_validation->set_rules('enderecoNovo', 'EnderecoNovo', 'required');
		$this->form_validation->set_rules('enderecoVelho', 'EnderecoVelho', 'required');
		
		if($this->form_validation->run())
		{	
			$data = array(
				'titulo'		=>	$this->input->post('titulo'),
				'descricao'		=>	$this->input->post('descricao'),
				'MinParticipantes'		=>	$this->input->post('MinParticipantes'),
				'encerramento'		=>	$this->input->post('encerramento'),
				'enderecoNovo'		=>	$this->input->post('enderecoNovo'),
				'enderecoVelho'		=>	$this->input->post('enderecoVelho'),
				'IdTask'			=>	$this->input->post('IdTask')
			);

			$this->Task_model->update_Task($data);

			$array = array(
				'success'		=>	true
			);
		}
		else
		{
			$array = array(
				'error'				=>	true,
				'titulo'	=>	form_error('titulo'),
				'descricao'	=>	form_error('descricao'),
				'MinParticipantes'	=>	form_error('MinParticipantes'),
				'encerramento'	=>	form_error('encerramento'),
				'enderecoNovo'	=>	form_error('enderecoNovo'),
				'enderecoVelho'	=>	form_error('enderecoVelho'),
				'IdTask'	=>	form_error('IdTask')
			);
		}
		echo json_encode($array);
	}

	function delete()
	{
		if($this->input->post('id'))
		{
		    $op = $this->Task_model->delete_Task($this->input->post('id'));
			if($op)
			{
				$array = array(
					'success'	=>	true
				);
			}
			else
			{
				$array = array(
					'error'		=>	true
				);
			}
			echo json_encode($array);
		}
	}
	
	function fetch_single()
	{
		if($this->input->post('id'))
		{
			$data = $this->Task_model->fetch_single_Task($this->input->post('id'));

			foreach($data as $row)
			{
				$output['titulo'] = $row['titulo'];
				$output['descricao'] = $row['descricao'];
				$output['categoria'] = $row['categoria'];
				$output['MinParticipantes'] = $row['MinParticipantes'];
				$output['encerramento'] = $row['encerramento'];
				$output['endereco'] = $row['endereco'];
				$output['IdUser'] = $row['IdUser'];
			}
			echo json_encode($output);
		}
	}

    function manage()
    {
        $this->form_validation->set_rules('IdUser', 'IdUser', 'required');
		$this->form_validation->set_rules('IdTask', 'IdTask', 'required');
		if($this->form_validation->run())
		{
			$data = array(
			    'IdUser'	=>	$this->input->post('IdUser'),
				'IdTask'	=>	$this->input->post('IdTask'),
			);

			$this->Task_model->manage_Task($data);

			$array = array(
				'success'		=>	true
			);
		}
		else
		{
			$array = array(
				'error'					=>	true,
				'IdUser'			=>	form_error('IdUser'),
				'IdTask'			=>	form_error('IdTask'),
			);
		}
		echo json_encode($array);
    }
    
    function adepto()
    {
        //echo("Recebido");
        $this->form_validation->set_rules('IdUser', 'IdUser', 'required');
		$this->form_validation->set_rules('IdTask', 'IdTask', 'required');
		if($this->form_validation->run())
		{
			$data = array(
			    'IdUser'	=>	$this->input->post('IdUser'),
				'IdTask'	=>	$this->input->post('IdTask'),
			);

			$resultado = $this->Task_model->adesao_Task($data);

			if ($resultado) {
			    $array = array(
    				'adepto'		=>	true
    			);
			} else {
			    $array = array(
    				'adepto'		=>	false
    			);
			}
			
		}
		else
		{
			$array = array(
				'error'					=>	true,
				'IdUser'			=>	form_error('IdUser'),
				'IdTask'			=>	form_error('IdTask'),
			);
		}
		echo json_encode($array);
    }

}

