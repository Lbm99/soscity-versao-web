<?php
header('Access-Control-Allow-Origin: *');
defined('BASEPATH') OR exit('No direct script access allowed');

include 'simplemail.php';
	    
class User extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');
		$this->load->library('form_validation');
	}


	function index()
	{
		$data = $this->user_model->fetch_all();
		echo json_encode($data->result_array());
	}

	function insert()
	{
		$this->form_validation->set_rules('nome', 'Nome', 'required');
		$this->form_validation->set_rules('cpf', 'CPF', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required');
		$this->form_validation->set_rules('endereco', 'Endereco', 'required');
		$this->form_validation->set_rules('nascimento', 'Nascimento', 'required');
		$this->form_validation->set_rules('senha', 'Senha', 'required');
        
		if($this->form_validation->run())
		{

			$data = array(
				'nome'	=>	$this->input->post('nome'), 
				'cpf'	=>	$this->input->post('cpf'), 
				'nascimento'=>	$this->input->post('nascimento'), 
				'email'	=>	$this->input->post('email'), 
				'senha'	=>	md5($this->input->post('senha')), 
				'IdExterno'		=>	$this->input->post('IdExterno'), 
				'endereco'	=>	$this->input->post('endereco')
			);
			
            
			$result = $this->user_model->insert_user($data);

            /*
            switch ($result){
                case "E-mail já cadastrado": 
                case "CPF já cadastrado": 
                    $array = array('error' => $result);
                    break;
                default: $array = array('success' => true);
             }
             */

			$array = array(
				'success'		=>	true, 'result' => $data
			);
		}
		else
		{
			$array = array(
				'error'					=>	true,
				'nome'			=>	form_error('nome'),
				'cpf'			=>	form_error('cpf'),
				'nascimento'	=>	form_error('nascimento'),
				'email'			=>	form_error('email'),
				'senha'			=>	form_error('senha'),
				'IdExterno'		=>	form_error('IdExterno'), 
				'endereco'		=>	form_error('endereco')

			);
		}
		echo json_encode($array);
	}
	
	function delete()
	{
	    $id = $this->input->post('id');
	    if($this->input->post('web')){ 
	        if(md5($this->input->post('velhasenha')) != $this->session->userdata('senha')){
	            echo "Senha Icorreta";
	            //redirect(base_url() . 'app/view/perfil');
	        } 
	        $id = $this->session->userdata('user');
	        
	    }
		if($id)
		{
		    $op = $this->user_model->delete_User($id);
			if($op)
			{
			    if($this->input->post('web')){
			        $this->session->sess_destroy();  
			        redirect(base_url() . 'app/view/login');
			    }
				$array = array(
					'success'	=>	true
				);
			}
			else
			{
				$array = array(
					'error'		=>	true
				);
			}
			echo json_encode($array);
		}
	}
	
    // método pega todas as informações de um usuário com base no CPF
    // conferir se a senha está certa -- retorno nada -- não cadastrado
    // se retornar - comparando com a senha 
	function search()
	{
		if($this->input->post('id'))
		{
			$data = $this->user_model->fetch_single_user_by_id($this->input->post('id'));

			foreach($data as $row)
			{
				$output['nome'] = $row['nome'];
				$output['email'] = $row['email'];
				$output['endereco'] = $row['coordenadas'];
				$output['nascimento'] = $row['nascimento'];
				$output['senha'] = $row['senha'];
				$output['IdExterno'] = $row['idExterno'];
			}
			echo json_encode($output);
		}
	}
	
	function login()
	{
		$this->form_validation->set_rules('cpf', 'CPF', 'required');
		$this->form_validation->set_rules('senha', 'Senha', 'required');

		if($this->form_validation->run())
		{
			$data = array(
				'cpf'=>$this->input->post('cpf'),
				'senha'=>$this->input->post('senha')
			);

			$result = $this->user_model->fetch_single_user_by_id($data['cpf']);
			
			if($result){
			    $array['user'] = $result[0]['idUser'];
    			$array['nome'] = $result[0]['nome'];
    			$array['cpf'] = $result[0]['cpf'];
    			$array['email'] = $result[0]['email'];
    			$array['endereco'] = $result[0]['idLocation'];
    			$array['coordenadas'] = $result[0]['coordenadas'];
    			$array['nascimento'] = $result[0]['nascimento'];
    			$array['senha'] = $result[0]['senha'];
    			$array['IdExterno'] = $result[0]['idExterno'];
    			//$array['failed'] = false;
    			//$array['error'] = false;
    			if($array['senha'] != md5($data['senha'])){
    			    $array = array(
        				'failed'		=>	"Senha Incorreta."
        			);
    			}
			} else {
			    $array = array(
        				'failed'		=>	"CPF não cadastrado");
			}
		}
		else
		{
			$array = array(
				'error'					=>	true,
				'cpf'			=>	form_error('cpf'),
				'senha'			=>	form_error('senha')
			);
		}
		//var_dump($array);
		if($this->input->post('web')){
		    if(!$array['failed'] && !$array['error']){
     		    //var_dump($array);
                $this->session->set_userdata($array);  
                redirect(base_url() . 'app/view/grid');
     		} else {
     		    $this->session->set_flashdata('error', 'Credenciais inválidas! Tente novamente.');  
                redirect(base_url() . 'app/view/login');
     		}
		} else echo json_encode($array);
	}
	
	function changep(){
	    
	    //$this->form_validation->set_rules('email', 'Email', 'required');
		$this->form_validation->set_rules('senha', 'Senha', 'required');
	    
	    $mail = $this->input->post('email');
	    
	    if($this->input->post('web')){
	        if(md5($this->input->post('velhasenha')) != $this->session->userdata('senha')) return "Senha atual incorreta";
	        if($this->input->post('senha') != $this->input->post('confsenha')) return "Confirme sua nova senha";
	        $mail = $this->session->userdata('email');   
	    }
	    
		if(!$mail){
		    echo json_encode("{'error': 'E-mail requerido'}");
		    return 0;
		}
		
		if($this->form_validation->run())
		{
			$data = array(
				'email'	=>	$mail, 
				'senha'	=>	$this->input->post('senha'), 
			);

			$this->user_model->change_user_pass($data);
			
		    mailPadrao($data);

			$array = array(
				'success'		=>	true
			);
		}
		else
		{
			$array = array(
				'error'					=>	true,
				//'email'			=>	form_error('email'),
				'senha'			=>	form_error('senha'),

			);
		}
		echo json_encode($array);
		if($this->input->post('web')) redirect(base_url() . 'app/view/perfil');
	}
	
	function changend(){
	    $this->form_validation->set_rules('coordenadasNovas', 'CoordenadasNovas', 'required');
	    
	    $id = $this->input->post('IdUser');
	    $enderecoVelho = $this->input->post('coordenadasVelhas');
	    
	    if($this->input->post('web')){
	        if($this->session->userdata('user')){
	            $id = $this->session->userdata('user');
	            $enderecoVelho = $this->session->userdata('coordenadas');
	        }
	        else return "Informações necessárias não foram coletadas";
	    }
		//$this->form_validation->set_rules('IdUser', 'IdUser', 'required');
		//$this->form_validation->set_rules('coordenadasVelhas', 'CoordenadasVelhas', 'required');
        
		if($this->form_validation->run())
		{
			$data = array(
				'coordenadasVelhas'	=>	$enderecoVelho, 
				'coordenadasNovas'	=>	$this->input->post('coordenadasNovas'), 
				'IdUser'	=>	$id, 
			);
			
			$this->user_model->change_user_location($data);

			$array = array(
				'success'		=>	true
			);
		}
		else
		{
			$array = array(
				'error'					=>	true,
				//'IdUser'			=>	form_error('IdUser'),
		//		'coordenadasVelhas'			=>	form_error('coordenadasVelhas'),
				'coordenadasNovas'			=>	form_error('coordenadasNovas'),

			);
		}
		echo json_encode($array);
		if($this->input->post('web')) redirect(base_url() . 'app/view/perfil');
	}
	
}


?>