<?php

  header('Content-type: text/html; charset=utf-8');
  setlocale( LC_ALL, 'pt_BR.utf-8');
  mb_internal_encoding('UTF8');
  mb_regex_encoding('UTF8');

  use PHPMailer\PHPMailer\PHPMailer;
  use PHPMailer\PHPMailer\Exception;
  use PHPMailer\PHPMailer\SMTP;

  require 'PHPMailer/src/Exception.php';
  require 'PHPMailer/src/PHPMailer.php';
  require 'PHPMailer/src/SMTP.php';

  function mailPadrao($data){
        $mailer = new PHPMailer();
        $mailer->IsSMTP();
        $mailer->SMTPDebug = 1;
        $mailer->Port = 587; //Indica a porta de conexão para a saída de e-mails. Utilize obrigatoriamente a porta 587.
        $mailer->Host = 'cpl04.main-hosting.eu'; //Onde em 'servidor_de_saida' deve ser alterado por um dos hosts abaixo:
        $mailer->SMTPSecure = 'tls';
        $mailer->SMTPAuth = true; //Define se haverá ou não autenticação no SMTP
        $mailer->Username = 'contato@aarc.com.br'; //Informe o e-mail o completo
        $mailer ->Username   = 'suporte@soscity.com.br';            // SMTP username
        $mailer ->Password   = 'fG6upaeH49@67Ma';                               // SMTP password
        $mailer->From = 'suporte@soscity.com.br'; //Obrigatório ser a mesma caixa postal indicada em "username"
        $mailer->FromName = 'SOSCITY'; //Nome que será exibido para o destinatário
        $mailer->Sender = 'suporte@soscity.com.br'; //Obrigatório ser a mesma caixa postal indicada em "username"
        $mailer->Priority = 1;
        $mailer->isHTML(true);
        $mailer->Subject = 'Alteracao de Senha SOSCITY';

         // destinatarios
         $mailer->AddAddress($_POST['email']); //Destinatários
         //$mailer->AddAddress('lbaradelmarchiori@gmail.com', 'Lucas'); // copia pra ver se esta tudo ok =)

         // mensagem
        $mailer->Body    = "<h1><center>Pedido de renovação de senha</center></h1>"
                          ."<br><p>Uma senha temporária foi disponibilizada para você, use-a para acessar o portal do SOSCITY e alterar sua senha.</p>"
                          ."<p>Sua senha temporária será válida até as 23:59 de sua data de solicitação.</p>"
                          ."<p>Sua senha temporária: ".$data['senha']."</p>"
                          ."<br><p>Caso você não tenha solicitado este pedido, por favor entre em contato para que possamos desvincular este entereo de e-mail de nosso sistema.</p>"
                          ."<br><h3>Contato:</h3><p><b>E-mail:</b> soscity@gmail.com</p>"
                          ."<br><h3>Obrigado!<br>SOSCITY</h3>";         
         $r = false;
         
        // tenta enviar
         if ($mailer->Send()) {
             $r = true;
         } else {
             $r = false;
         }
    
         // Limpa os destinatários e os anexos
         $mailer->ClearAllRecipients();
         $mailer->ClearAttachments();
         
         return $r;
	    
  }

  
?>
