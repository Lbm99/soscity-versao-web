<?php
class Task_model extends CI_Model
{
	function fetch_all()
	{
	    /*
		$this->db->order_by('idTask', 'DESC');
		return $this->db->get('Task');
		*/
		$result = $this->db->query("CALL buscaChamadas");
        if ($result) {
            return $result;
        }
	}

	function insert_Task($data)
	{
		//$this->db->insert('Task', $data);
		$result = $this->db->query("CALL cadastraChamada(?, ?, ?, ?, ?, ?, ?)", $data);
        if ($result) {
            return $result;
        }
	}
	
	function update_Task($data)
	{
	    //var_dump($data);
		//$this->db->where('id', $task_id);
		//$this->db->update('tbl_sample', $data);
		$array1 = [$data['enderecoVelho'], $data['enderecoNovo'], $data['IdTask']];
		$array2 = [$data['titulo'], $data['descricao'], $data['MinParticipantes'], $data['encerramento'], $data['IdTask']];
	    $result = $this->db->query("CALL atualisaEnderecoTask(?, ?, ?)", $array1);
	    $result = $this->db->query("UPDATE Task SET titulo = ?, descricao = ?, MinParticipantes = ?, encerramento = ? WHERE idTask = ?;", $array2);
	}

	function delete_Task($task_id)
	{
		$this->db->where('idTask', $task_id);
		$this->db->delete('Task');
		if($this->db->affected_rows() > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function manage_Task($data)
	{
		//$this->db->insert('Task', $data);
		$adesao = $this->adesao_Task($data);
		if ($adesao) {
		    //echo(2);
		    $result = $this->db->query("DELETE FROM Task_Force WHERE idTask_Force = ?", $adesao->idTask_Force);
		} else {
		    //echo(3);
		    $result = $this->db->query("INSERT INTO Task_Force VALUES (NULL, ?, ?, 2)", $data);
		}
        if ($result) {
            return $result;
        }
	}
	
	function adesao_Task($data)
	{
		//$this->db->insert('Task', $data);
		$result = $this->db->query("SELECT idTask_Force FROM Task_Force WHERE idUser = ? AND idTask = ? LIMIT 1", $data);
        if ($result) {
            //echo(1);
            return $result->row();
        } else {
            return false;
        }
	}
	
	function search_Task($data)
	{
		//$this->db->insert('Task', $data);
		$result = $this->db->query("SELECT * FROM Task WHERE titulo LIKE '%?%' ORDER BY titulo", $data);
        if ($result) {
            //echo(1);
            return $result->row();
        } else {
            return false;
        }
	}

	function fetch_single_Task($Task_id)
	{
		$this->db->where('idTask', $Task_id);
		$query = $this->db->get('Task');
		return $query->result_array();
	}
	
	function fetch_user($data)
	{
	    //Melhoria futura: transformar esta query em uma View
        $result = $this->db->query("SELECT * FROM Task T INNER JOIN Task_Force TF ON T.idTask = TF.idTask INNER JOIN Task_has_Location TL ON T.idTask = TL.idTask INNER JOIN Location L ON TL.idLocation = L.idLocation WHERE TF.idUser = ? AND TF.status = 1", $data);
        if ($result) {
            return $result;
        }
	}

}

?>