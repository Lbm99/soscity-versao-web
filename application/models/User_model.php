<?php
class User_model extends CI_Model
{
	function fetch_all()
	{
		$this->db->order_by('idUser', 'DESC');
		return $this->db->get('User');
	}

	function insert_user($data)
	{
	   // echo "Cadastrou";
	   // $result = $this->db->query("SELECT * FROM User WHERE email = ?", $data['email']);
    //     if ($result) {
    //         return "E-mail já cadastrado";
    //     }
        
    //     $result = $this->db->query("SELECT * FROM User WHERE cpf = ?", $data['cpf']);
    //     if ($result) {
    //         return "CPF já cadastrado";
    //     }
	    
        $result = $this->db->query("CALL cadastraUser(?, ?, ?, ?, ?, ?, ?)", $data);
        if ($result) {
            return $result;
        }
	}
	
	function delete_User($user_id)
	{
		$this->db->where('idUser', $user_id);
		$this->db->delete('User');
		if($this->db->affected_rows() > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	function fetch_single_user_by_id($user_id)
	{
		$query = $this->db->query("SELECT * FROM User INNER JOIN Location ON idLocation = endereco WHERE cpf = ?", $user_id);
		return $query->result_array();
	}


	function verifica_Login()
	{
	    $cpf = $_POST['cpf'];
        $user = fetch_single_user_by_id($cpf);
        
        
        if (!$user) throw new Exception('Verifique se seu CPF foi digitado corretamente.');
        
        echo($user);
	}
	
	function change_user_pass($data)
	{
	    $senha = md5($data['senha']);
	    $email = $data['email'];
	    
	    //caso nao exista usuário com email informado
        // $u = $this->db->query( "SELECT * FROM User u WHERE u.email = '$email'" );
        // if ($u->num_rows() == 0) throw new Exception('Usuario inexistente com o e-mail informado.');
        // $u = $u->row_array();
        
        $result = $this->db->query("UPDATE User SET senha = '$senha' where email = '$email'");
        if ($result) {
            return $result;
        }
        
	}
	
	function change_user_location($data)
	{
        $result = $this->db->query("CALL atualisaEnderecoUser(?, ?, ?)", $data); //Enderço antigo e Endereço novo, respectivamente
        if ($result) {
            return $result;
        }
	}
}

?>